package com.example.uploadimage;

import android.app.Application;

import cn.bmob.v3.Bmob;

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        //第一：默认初始化
//        Bmob.initialize(this, "Your Application ID");
        Bmob.initialize(this, "4626b385edd78ca96f3d18708078776b");
    }
}
